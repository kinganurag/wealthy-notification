# -*- coding: <utf-8> -*-

# Date: Jan 15, 2015
# Title: Async Notification
# Description: Notification project for wealty


from flask import Flask, render_template, request, session, redirect, flash, url_for, jsonify
from celery import Celery
# import os
from packages.Mailer import Mailer
import logging



# ------ Logging code ------ #
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
handler = logging.FileHandler('logs/notification.log')
handler.setLevel(logging.INFO)
formatter = logging.Formatter(
    '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)
# ------- Logging code end --- #


app = Flask(__name__)
app.config.update(
    SECRET_KEY='NA8GFJAO##H^&RO**(*(I0098}{QM0[]9MO(!!G'
)
app.config['CELERY_BROKER_URL'] = 'redis://localhost:6379/0'
app.config['CELERY_RESULT_BACKEND'] = 'redis://localhost:6379/0'

celery = Celery(app.name, broker=app.config['CELERY_BROKER_URL'])
celery.conf.update(app.config)


@celery.task
def send_async_email(sender_email, recipients_email_list):
    """Background task to send an email."""
    with app.app_context():
        logger.info('Sending Mail to user')
        msg = render_template("email_template.html")

        result = Mailer().py_mail(
            "Notification from wealthy", msg, recipients_email_list, sender_email)
        print result


@app.route('/', methods=['GET', 'POST'])
def index():
    logger.info('Notification get request')
    if request.method == 'GET':
        logger.info('Notification get request')
        return render_template('email.html', email=session.get('email', ''))
    email = request.form['email']
    session['email'] = email

    if request.form['submit'] == 'Send':
        # send right away
        # print "sending right"

        send_async_email.delay("no-reply@wealthy.in", email)
        # print "async"
        flash('Sending email to {0}'.format(email))

    return redirect(url_for('index'))


@app.route('/mailtest', methods=['GET', 'POST'])
def mailtest():
    print "mail start"
    logger.info('Mail testing function called')
    print request.args
    sender_email = "no-reply@wealthy.in"
    recipients_email_list = request.args.get("email", None)
    msg = render_template("email_template.html")
    print "start of mail"
    if recipients_email_list:
        result = Mailer().py_mail(
            "Notification from wealthy", msg, recipients_email_list, sender_email)
    return jsonify(result)


if __name__ == '__main__':
    app.debug = True
    app.run(host='0.0.0.0', port=8088)
