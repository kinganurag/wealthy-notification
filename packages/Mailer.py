import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import logging

# ------ Logging code ------ #
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
handler = logging.FileHandler('logs/mailer.log')
handler.setLevel(logging.INFO)
formatter = logging.Formatter(
    '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)
# ------- Logging code end --- #


class Mailer():

    def smtp_mailer(self, sender, receivers, msg):
        """
        Usage:
            result = Mailer().smtp_mailer('kinganurag@hotmail.com', ['xyz@gmail.com'])
        """

        try:
            smtpObj = smtplib.SMTP('localhost')
            smtpObj.sendmail(sender, receivers, msg)
            return "Successfully sent email"
        except SMTPException:
            return "Error: unable to send email"

    def py_mail(self, SUBJECT, BODY, TO, FROM):
        """
        Usage:
        email_content = "
                    <head>
                      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
                      <title>html title</title>
                      <style type="text/css" media="screen">
                        table{
                            background-color: #AAD373;
                            empty-cells:hide;
                        }
                        td.cell{
                            background-color: white;
                        }
                      </style>
                    </head>
                    <body>
                      <table style="border: blue 1px solid;">
                        <tr><td class="cell">Cell 1.1</td><td class="cell">Cell 1.2</td></tr>
                        <tr><td class="cell">Cell 2.1</td><td class="cell"></td></tr>
                      </table>
                    </body>
                    "

        TO = 'garuna1008@gmail.com'
        FROM = 'no-reply@mysite.com'

        result = Mailer().py_mail("Test email subject", email_content, TO, FROM)

        """

        """With this function we send out our html email"""

        # Create message container - the correct MIME type is
        # multipart/alternative here!
        print "pymail called"
        MESSAGE = MIMEMultipart('alternative')
        MESSAGE['subject'] = SUBJECT
        MESSAGE['To'] = TO
        MESSAGE['From'] = FROM
        MESSAGE.preamble = """
        Your mail reader does not support the report format.
        Please visit us <a href="http://wealthy.in">online</a>!"""

        HTML_BODY = MIMEText(BODY, 'html')

        MESSAGE.attach(HTML_BODY)

        # The actual sending of the e-mail
        server = smtplib.SMTP('localhost')
        try:

            server.sendmail(FROM, [TO], MESSAGE.as_string())
        except Exception as e:
            print e
            logging.warning(e)

        server.quit()
        print "mail sent"
        return {"success": 1}

